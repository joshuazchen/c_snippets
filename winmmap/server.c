#include <windows.h>
#include <stdio.h>
#include "const.h"

HANDLE CreateMap(void)
{
	HANDLE MemMap;

	MemMap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE,
							   0, MAPSIZE, MAPNAME);
	return MemMap;
}

int main(void)
{
	HANDLE MemMap;
	char *MapAdd;
	char str[MAPSIZE] = "Hello";

	MemMap = CreateMap();
	if (!MemMap) {
		printf("CreateMap() error!\n");
		return -1;
	}

	MapAdd = MapViewOfFile(MemMap, FILE_MAP_WRITE|FILE_MAP_READ, 0, 0, MAPSIZE);
	if (!MapAdd) {
		printf("MapViewOfFile() error!\n");
		return -1;
	}

	memcpy(MapAdd, str, sizeof(str));

	Sleep(5000);
	return 0;
}
