#include <windows.h>
#include <stdio.h>
#include "const.h"

HANDLE OpenMap(void)
{
	HANDLE MemMap;

	MemMap = OpenFileMapping(FILE_MAP_READ|FILE_MAP_WRITE, FALSE, MAPNAME);
	return MemMap;
}

int main(void)
{
	HANDLE MemMap;
	char *MapAdd;

	MemMap = OpenMap();
	if (!MemMap) {
		printf("OpenMap() error!\n");
		getchar();
		return -1;
	}

	MapAdd = MapViewOfFile(MemMap, FILE_MAP_WRITE|FILE_MAP_READ, 0, 0, MAPSIZE);
	if (!MapAdd) {
		printf("MapViewOfFile() error!\n");
		getchar();
		return -1;
	}

	while (1) {
		printf("%s\n", MapAdd);
		Sleep(2000);
	}
	return 0;
}
