char SourceCode[] = {
"#include <windows.h>\n\
#include <stdio.h>\n\
#include <time.h>\n\
#include <mmsystem.h>\n\
\n\
#define LCID_CHS 0x804\n\
#define LANG_CHS 0\n\
#define LANG_ENG 1\n\
\n\
extern char SourceCode[];\n\
\n\
// 帮助信息\n\
const char *HelpInfo = {\n\
	\"Notifier, version 1.03, 12/07/2012.\\n\"\n\
	\"Will beep at minute 55, 00, 05, 10 every hour.\\n\\n\"\n\
	\"-h    display this help and exit\\n\"\n\
	\"-s    display the source code and exit\\n\"\n\
};\n\
\n\
// 运行时的提示信息\n\
const char *MsgRunning[] = {\n\
	\"程序正在运行 ...\\n\\n\"\n\
	\"每小时的55分、00分、05分、10分将各播放一次提示音。\\n\"\n\
	\"带参数 \\\"-h\\\" 运行程序以查看帮助信息。\\n\\n\",\n\
	\"The program is running ...\\n\\n\"\n\
	\"The sound will be played at minute 55, 00, 05, 10 every hour.\\n\"\n\
	\"Run this program with argument \\\"-h\\\" to see the help.\\n\\n\"\n\
};\n\
\n\
// 获取当前时间\n\
struct tm *GetTime(void) {\n\
	time_t timep;\n\
	time(&timep);\n\
	return localtime(&timep);\n\
}\n\
\n\
// 检查当前是否应该播放提示音\n\
int CheckTime(struct tm *ptm) {\n\
	static int MinuteLast = -1;\n\
	int ret = 0;\n\
	int MinuteNow;\n\
\n\
	MinuteNow = ptm->tm_min;\n\
	if (MinuteLast != MinuteNow) {\n\
		switch (MinuteNow) {\n\
		case 0:\n\
		case 5:\n\
		case 10:\n\
		case 55:\n\
			MinuteLast = MinuteNow;\n\
			ret = 1;\n\
			break;\n\
		default:\n\
			break;\n\
		}\n\
	}\n\
\n\
	return ret;\n\
}\n\
\n\
int main(int argc, char *argv[]) {\n\
	struct tm *ptm;\n\
\n\
	// 参数处理\n\
	if (argc > 1) {\n\
		if (!strncmp(argv[1], \"-h\", 2)) {\n\
			printf(\"Usage: %s [OPTION]\\n\", argv[0]);\n\
			printf(\"%s\\n\", HelpInfo);\n\
			return 0;\n\
		} else if (!strncmp(argv[1], \"-s\", 2)) {\n\
			printf(\"%s\\n\", SourceCode);\n\
			return 0;\n\
		}\n\
	}\n\
	\n\
	// 检查操作系统语言是否为简体中文\n\
	if (GetSystemDefaultLCID() == LCID_CHS) {\n\
		printf(\"%s\\n\", MsgRunning[LANG_CHS]);\n\
	} else {\n\
		printf(\"%s\\n\", MsgRunning[LANG_ENG]);\n\
	}\n\
\n\
	while (true) {\n\
		// 显示时间\n\
		ptm = GetTime();\n\
		printf(\"\\r%02d:%02d:%02d\", ptm->tm_hour, ptm->tm_min, ptm->tm_sec);\n\
		// 播放提示音\n\
		if (CheckTime(ptm)) {\n\
			sndPlaySound(MAKEINTRESOURCE(1), SND_ASYNC|SND_RESOURCE);\n\
		}\n\
		Sleep(500);\n\
	}\n\
\n\
	return 0;\n\
}\n"
};
