#include <windows.h>
#include <stdio.h>
#include <time.h>
#include <mmsystem.h>

#define LCID_CHS 0x804
#define LANG_CHS 0
#define LANG_ENG 1

extern char SourceCode[];

// 帮助信息
const char *HelpInfo = {
	"Notifier, version 1.04, 12/10/2012.\n"
	"Will beep at minute 55, 00, 05, 10 every hour.\n\n"
	"-h    display this help and exit\n"
	"-s    display the source code and exit\n"
};

// 运行时的提示信息
const char *MsgRunning[] = {
	"程序正在运行 ...\n\n"
	"每小时的55分、00分、05分、10分将各播放一次提示音。\n"
	"带参数 \"-h\" 运行程序以查看帮助信息。\n\n",
	"The program is running ...\n\n"
	"The sound will be played at minute 55, 00, 05, 10 every hour.\n"
	"Run this program with argument \"-h\" to see the help.\n\n"
};

// 获取当前时间
struct tm *GetTime(void) {
	time_t timep;
	time(&timep);
	return localtime(&timep);
}

// 检查当前是否应该播放提示音
int CheckTime(struct tm *ptm) {
	static int MinuteLast = -1;
	int ret = 0;
	int MinuteNow;

	MinuteNow = ptm->tm_min;
	if (MinuteLast != MinuteNow) {
		switch (MinuteNow) {
		case 0:
		case 5:
		case 10:
		case 55:
			MinuteLast = MinuteNow;
			ret = 1;
			break;
		default:
			break;
		}
	}

	return ret;
}

int main(int argc, char *argv[]) {
	struct tm *ptm;

	// 参数处理
	if (argc > 1) {
		if (!strncmp(argv[1], "-h", 2)) {
			printf("Usage: %s [OPTION]\n", argv[0]);
			printf("%s\n", HelpInfo);
			return 0;
		} else if (!strncmp(argv[1], "-s", 2)) {
			printf("%s\n", SourceCode);
			return 0;
		}
	}
	
	// 检查操作系统语言是否为简体中文
	if (GetSystemDefaultLCID() == LCID_CHS) {
		printf("%s\n", MsgRunning[LANG_CHS]);
	} else {
		printf("%s\n", MsgRunning[LANG_ENG]);
	}

	while (1) {
		// 显示时间
		ptm = GetTime();
		printf("\r%02d:%02d:%02d", ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
		// 播放提示音
		if (CheckTime(ptm)) {
			sndPlaySound(MAKEINTRESOURCE(1), SND_ASYNC|SND_RESOURCE);
		}
		Sleep(500);
	}

	return 0;
}
