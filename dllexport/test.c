#include <stdio.h>
#include "line_count.h"

int main(int argc, char *argv[])
{
    printf("argv[1]: %s\n", argv[1]);
    printf("line count: %d\n", line_count(argv[1]));
    return 0;
}
