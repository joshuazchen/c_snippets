1. compile test.c
gcc -c test.c

2. compile line_count.c
gcc -c -DBUILD_DLL line_count.c

3. create line_count.dll
gcc -shared -o line_count.dll line_count.o -Wl,--out-implib,libline_count.a

4. create exe file
gcc -o test.exe test.o -L./ -lline_count
