#include <stdio.h>
#include <string.h>
#include "line_count.h"

#define LINE_SIZE 1024

EXPORT int line_count(char *path)
{
    int count;
    FILE *fd;
    char buf[LINE_SIZE];

    count = 0;
    memset(buf, 0, sizeof(buf));
    fd = fopen(path, "r");
    if (fd) {
        while (fgets(buf, LINE_SIZE, fd))
            count++;
    }
    printf("path: %s\n", path);
    return count;
}
