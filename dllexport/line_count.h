#ifndef _LINE_COUNT_H
#define _LINE_COUNT_H

#ifdef BUILD_DLL
// dll export
#define EXPORT __declspec(dllexport)
#else
// exe import
#define EXPORT __declspec(dllimport)
#endif

EXPORT int line_count(char *path);

#endif
