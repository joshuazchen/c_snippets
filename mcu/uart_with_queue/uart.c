/* ****************************************************************
 * File Name: uart.c
 * Function:  AVR UART I/O 功能模块
 * Description: 实现UART输入/回显/格式化输出功能, 便于通过终端软件人机交互
 * Authon & Date: Joshua Chan, 2012/04/01
 * ****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ioavr.h>
#include <intrinsics.h>
#include <comp_A90.h>
#include <avr_macros.h>
#include "uart.h"

static u8 rx_buf[RX_BUF_SIZE];
static u8 tx_buf[TX_BUF_SIZE];
queue_st rx_queue;
static queue_st tx_queue;
static bool print_mode;       /* 发送数据时为false, 发送可打印信息为true */

/* UART初始化 */
void uart_init(u32 baud)
{
    u16 ubrr;

    print_mode = false;
    queue_init(&rx_queue, rx_buf, RX_BUF_SIZE);
    queue_init(&tx_queue, tx_buf, TX_BUF_SIZE);
    /* 设置baudrate */
    ubrr = (u16)((FOSC / (8 * baud)) - 1);
    UBRR0H = (u8)(ubrr >> 8);
    UBRR0L = (u8)ubrr;
    /* 倍速发送 */
    UCSR0A |= 1 << U2X0;
    /* 设置帧格式: 8个数据位, 1个停止位 */
    UCSR0C = 3 << UCSZ00;
    /* 使能: 接收结束中断, 接收, 发送 */
    UCSR0B = (1<<RXCIE0) | (1<<RXEN0) | (1<<TXEN0);
}

/* 接收结束中断处理函数 */
#pragma vector = USART0_RXC_vect
__interrupt void uart_rxc_isr_orig(void)
{
    u8 udr0 = UDR0;
    queue_put(&rx_queue, udr0);
    rx_cmd_format_check(udr0);
}

u8 tx_data_left;

/* 数据寄存器空中断处理函数 */
#pragma vector = USART0_UDRE_vect
__interrupt void uart_udre_isr_orig(void)
{
    if (queue_not_empty(&tx_queue))
        UDR0 = queue_get(&tx_queue);
    else
        UCSR0B &= ~(1 << UDRIE0);
    //uart_udre_isr();
}

/* 读1字节, 无数据则忙等 */
u8 uart_getchar(void)
{
    while (!queue_not_empty(&rx_queue))
        _WDR();
    return queue_get(&rx_queue);
}

/* 尝试读1字节, 无数据返回0, 可用于检测按键 */
u8 uart_getkey(void)
{
    if (queue_not_empty(&rx_queue))
        return queue_get(&rx_queue);
    else
        return 0;
}

/* 接收字符串, 并回显, 需在参数中给定字符串存放位置 */
void uart_getstring(u8 *str)
{
    u8 c;
    u8 *s = str;

    while ((c = uart_getchar()) != '\r') {
        if (c == '\b') {
            if ((str - s) > 0) {
                uart_putstring("\b \b");
                str--;
            }
        } else {
            *str = c;
            str++;
            uart_putchar(c);
        }
    }
    *str = '\0';
    uart_putchar('\n');
}

/* 接收整型数, 支持8进制/10进制/16进制输入 */
u16 uart_getnum(void)
{
    u8 str[10];

    uart_getstring(str);
    return (u16)strtol((char *)str, NULL, 0);
}

/* 写1字节, 发送缓冲器满则忙等 */
void uart_putchar(u8 data)
{
    while (!queue_not_full(&tx_queue))
        ;
    queue_put(&tx_queue, data);
    if (print_mode && data == '\n')
        uart_putchar('\r');
    /* 使能中断, 以便自动发送 */
    UCSR0B |= (1 << UDRIE0);
}

/* 字符串输出 */
void uart_putstring(u8 *str)
{
    if (!print_mode)
        print_mode = true;
    while (*str)
        uart_putchar(*str++);
}

/* 格式化输出, 不支持浮点数 */
void uart_printf(const u8 *fmt, ...)
{
    u8 str[128];         /* 数组空间尽可能大一些, 以免越界 */
    va_list args;

    va_start(args, fmt);
    vsprintf((char *)str, (char *)fmt, args);
    uart_putstring(str);
    va_end(args);
}
