/* *************************************************
 * File Name: timer0_set.h
 * Function:  AVR定时器0配置模块
 * Description: 指定期望的定时器中断频率，及中断处理函数，
                模块可自动完成配置
 * Authon & Date: Joshua Chan, 2012/04/01
 * *************************************************/
#ifndef _TIMER0_SET_H
#define _TIMER0_SET_H

#define COUNT_MAX 0xFF

/* 中断处理函数由外部定义 */
extern void timer0_isr(void);

/* 配置8位定时器timer0比较匹配中断使能
 * @freq: 定时器中断发生频率
 */
extern void timer0_comp_init(int freq);

extern void timer0_enable(void);

extern void timer0_disable(void);

#endif
