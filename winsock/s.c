// t1.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include <stdio.h>
#include <winsock.h>

#define PORT_NUM 8080

int main(int argc, char* argv[])
{
	WORD sock_ver;
	WSADATA wsadata;
	int sockfd;
	int newfd;
	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;
	int sin_size;
	int nbytes;
	char buffer[1024];

	sock_ver = MAKEWORD(1, 1); /* 使用Winsock 1.1版本 */
	/* Winsock初始化 */
	if (WSAStartup(sock_ver, &wsadata)) {
		fprintf(stderr, "WSAStartup() error!\n");
		return -1;
	}

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockfd == -1) {
		fprintf(stderr, "socket() error!\n");
		return -1;
	}

	/* 服务器端填充SOCKADDR_IN结构 */
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
//	server_addr.sin_addr.s_addr = inet_addr("192.168.1.1");
	server_addr.sin_port = htons(PORT_NUM);

	if (bind(sockfd, (struct sockaddr *)(&server_addr), sizeof(server_addr)) == -1) {
		fprintf(stderr, "bind() error!\n");
		return -1;
	}

	/* 进入监听模式 */
	if (listen(sockfd, 5) == -1) {
		fprintf(stderr, "listen() error!\n");
		return -1;
	}

	unsigned long mode;

	while(1)
	{
		/* 服务器阻塞,直到客户程序建立连接 */
		sin_size = sizeof(struct sockaddr_in);
		newfd = accept(sockfd, (struct sockaddr *)(&client_addr), &sin_size);
		if (newfd == -1) {
			fprintf(stderr, "accept() error!\n");
			return -1;
		}

		printf("Server get connection from %s\n", inet_ntoa(client_addr.sin_addr));

		mode = 1; /* 非阻塞模式 */
		ioctlsocket(newfd, FIONBIO, &mode);

		Sleep(5000);
		nbytes = recv(newfd, buffer, 1024, 0);
//		if (nbytes == -1) {
//			fprintf(stderr,"read() error!\n");
//			return -1;
//		}

		printf("ret recv: %d\n", nbytes);

//		buffer[nbytes]='\0';
//		printf("Server received %s\n", buffer);

		closesocket(newfd);
	}

	closesocket(sockfd);

	WSACleanup();

	return 0;
}
