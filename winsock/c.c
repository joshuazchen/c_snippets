// t1.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include <stdio.h>
#include <winsock.h>

#define PORT_NUM 8080

int main(int argc, char* argv[])
{
	WORD sock_ver;
	WSADATA wsadata;
	int sockfd;
	struct sockaddr_in server_addr;
	int nbytes;
	char buffer[1024];

	sock_ver = MAKEWORD(1, 1); /* 使用Winsock 1.1版本 */
	/* Winsock初始化 */
	if (WSAStartup(sock_ver, &wsadata)) {
		fprintf(stderr, "WSAStartup() error!\n");
		return -1;
	}

	sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sockfd == -1) {
		fprintf(stderr, "socket() error!\n");
		return -1;
	}

	/* 服务器端填充SOCKADDR_IN结构 */
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	server_addr.sin_port = htons(PORT_NUM);

	if (connect(sockfd, (struct sockaddr *)(&server_addr), sizeof(server_addr))) {
		fprintf(stderr, "connect() error!\n");
		return -1;
	}

	unsigned long mode;
	mode = 1; /* 非阻塞模式 */
	ioctlsocket(sockfd, FIONBIO, &mode);

	printf("Please input char1:\n");
	fgets(buffer,1024,stdin);
	printf("ret send1: %d\n", send(sockfd, buffer, 128, 0));

	printf("Please input char2:\n");
	fgets(buffer,1024,stdin);
	printf("ret send2: %d\n", send(sockfd, buffer, 128, 0));
	
	closesocket(sockfd);

	WSACleanup();

	getchar();

	return 0;
}
