/* ************************************
 * Name: btn_int_drv.c
 * Proj: 实现中断方式按键驱动程序(Mini2440)
 * Auth & Date: Joshua Chan, 2011/11/29
 * ***********************************/
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/wait.h>
#include <linux/uaccess.h>
#include <mach/regs-gpio.h>
#include <mach/hardware.h>

/* Mini2440的按键key1~6, 对应端口为GPG 0, 3, 5, 6, 7, 11,
   对应中断为EINT 8, 11, 13, 14, 15, 19 */

/* 为了使驱动中的read()函数在没有中断发生的时候休眠, 需先定义一个等待队列 */
static DECLARE_WAIT_QUEUE_HEAD(btn_wq);

/* 定义等待队列的唤醒条件 */
static volatile int btn_press;

/* 读取相应键值, 即GPGDAT相应位的值, 通过键值可以判断按下/抬起状态 */
static unsigned int key_val;

/* 由于这里使用内核函数s3c2410_gpio_getpin()读取GPGDAT的值,
   所以需要提供各按键的pin值, 将其定义为一个数组, 并作为request_irq()
   的参数传入 */
static unsigned int pin[6] = {
    S3C2410_GPG0,
    S3C2410_GPG3,
    S3C2410_GPG5,
    S3C2410_GPG6,
    S3C2410_GPG7,
    S3C2410_GPG11,
};

/* 定义中断处理函数, 读取中断按键的状态值, 并唤醒等待队列 */
static irqreturn_t btn_isr(int irq, void *pin)
{
    key_val = s3c2410_gpio_getpin(*(unsigned int *)pin);
    printk("irq = %d, pin = 0x%x, key_val = 0x%x\n", irq, *(unsigned int *)pin, key_val);

    btn_press = 1;
    wake_up_interruptible(&btn_wq);

    return 0;
}

/* 注册中断, 完成申请中断号, 向内核加入中断处理函数, 设定中断触发条件等任务,
   注册成功后可在/proc/interrupts中查看 */
static int btn_open(struct inode *inode, struct file *file)
{
    request_irq(IRQ_EINT8,  btn_isr, IRQ_TYPE_EDGE_RISING, "my_button1", &pin[0]);
    request_irq(IRQ_EINT11, btn_isr, IRQ_TYPE_EDGE_RISING, "my_button2", &pin[1]);
    request_irq(IRQ_EINT13, btn_isr, IRQ_TYPE_EDGE_RISING, "my_button3", &pin[2]);
    request_irq(IRQ_EINT14, btn_isr, IRQ_TYPE_EDGE_RISING, "my_button4", &pin[3]);
    request_irq(IRQ_EINT15, btn_isr, IRQ_TYPE_EDGE_RISING, "my_button5", &pin[4]);
    request_irq(IRQ_EINT19, btn_isr, IRQ_TYPE_EDGE_RISING, "my_button6", &pin[5]);

    return 0;
}

/* 定义驱动中的read()函数, 将中断按键状态传回用户空间, 无中断发生则休眠 */
static ssize_t btn_read(struct file *file, char __user *buf,
                        size_t count, loff_t *loff)
{
    wait_event_interruptible(btn_wq, btn_press);
    btn_press = 0;
    copy_to_user(buf, &key_val, 4);

    return 4;
}

/* 驱动中的release()函数, 注销中断 */
static int btn_close(struct inode *inode, struct file *file)
{
    free_irq(IRQ_EINT8,  &pin[0]);
    free_irq(IRQ_EINT11, &pin[1]);
    free_irq(IRQ_EINT13, &pin[2]);
    free_irq(IRQ_EINT14, &pin[3]);
    free_irq(IRQ_EINT15, &pin[4]);
    free_irq(IRQ_EINT19, &pin[5]);

    return 0;
}

/* 驱动中的设备操作方法结构体 */
static struct file_operations btn_fops = {
    .owner = THIS_MODULE,
    .open = btn_open,
    .read = btn_read,
    .release = btn_close,
};

static int major;                 // 主设备号
static struct class *btn_class;   // 设备类
static struct device *btn_device; // 设备结构

/* 驱动入口函数, 完成驱动注册, 设备类申请, 设备自动生成工作 */
static __init int btn_init(void)
{
    major = register_chrdev(0, "my_buttons", &btn_fops);
    btn_class = class_create(THIS_MODULE, "my_buttons");
    btn_device = device_create(btn_class, NULL, MKDEV(major, 0), NULL, "my_buttons");

    return 0;
}

/* 驱动出口函数, 完成驱动清理工作 */
static __exit void btn_exit(void)
{
    unregister_chrdev(major, "my_buttons");
    device_destroy(btn_class, MKDEV(major, 0));
    class_destroy(btn_class);
}

module_init(btn_init);
module_exit(btn_exit);
MODULE_LICENSE("GPL");