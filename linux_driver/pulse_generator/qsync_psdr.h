/* *******************************************************************
 * File Name: qsync_psdr.h
 * Function:  驱动S3C6410的GPQ4,5,6端口(对应偏高,偏低,合闸)产生脉冲
 * Description: 1. 通过ioctl方法完成以下工作:
 *                 设置脉冲的宽度, 周期, 类型, 开始发送, 停止发送, 读脉宽及周期.
 *              2. 脉冲宽度及周期未设置时, 默认值为0, 当脉冲周期小于脉宽时, 脉冲
 *                 只发送一次, 否则持续发送.
 *              3. 接收到停止发送的命令后, 当前脉冲会立即停止.
 *              4. 接收到开始发送的命令后, 新脉冲会立即启动.
 * Author & Date: Joshua Chan, 2011/12/26
 * ******************************************************************/
#ifndef _QSYNC_PSDR_H
#define _QSYNC_PSDR_H

#include <linux/ioctl.h>

#define PULSE_DEVICE_NAME  "qsync_psdr"      //设备名
#define PULSE_DEVICE_PATH  "/dev/qsync_psdr" //设备文件路径

#define HIGHER_PULSE_PORT  4  //频率偏高时GPQ4端口产生脉冲
#define LOWER_PULSE_PORT   5  //频率偏低时GPQ5端口产生脉冲
#define CLOSING_PULSE_PORT 6  //合闸脉冲由GPQ6端口发生

/* 调频脉冲类型 */
#define FM_TYPE_HIGHER  0  //偏高
#define FM_TYPE_LOWER   1  //偏低

enum pulse_gen_ioc_cmd {
    //单位为ms, 若不设周期, 默认只发送一次脉冲
    PS_IOC_MAGIC = 'S',

    //控制调频脉冲发送的ioctl命令
    PS_FM_TYPE_GET   = _IOR(PS_IOC_MAGIC, 0, unsigned long),
    PS_FM_WIDTH_GET  = _IOR(PS_IOC_MAGIC, 1, unsigned long),
    PS_FM_TYPE_SET   = _IOR(PS_IOC_MAGIC, 2, unsigned long),
    PS_FM_WIDTH_SET  = _IOR(PS_IOC_MAGIC, 3, unsigned long),
    PS_FM_CYCLE_SET  = _IOR(PS_IOC_MAGIC, 4, unsigned long),
    PS_FM_CYCLE_GET  = _IOR(PS_IOC_MAGIC, 5, unsigned long),
    PS_FM_START      = _IOR(PS_IOC_MAGIC, 6, unsigned long),
    PS_FM_STOP       = _IOR(PS_IOC_MAGIC, 7, unsigned long),

    //控制合闸脉冲发送的ioctl命令
    PS_CS_WIDTH_SET  = _IOR(PS_IOC_MAGIC, 20, unsigned long),
    PS_CS_WIDTH_GET  = _IOR(PS_IOC_MAGIC, 21, unsigned long),
    PS_CS_CYCLE_SET  = _IOR(PS_IOC_MAGIC, 22, unsigned long),
    PS_CS_CYCLE_GET  = _IOR(PS_IOC_MAGIC, 23, unsigned long),
    PS_CS_START  = _IOR(PS_IOC_MAGIC, 24, unsigned long),
    PS_CS_STOP  = _IOR(PS_IOC_MAGIC, 25, unsigned long),
};

#endif