/* ************************************
 * File Name: test.c
 * Function:  驱动S3C6410的GPQ端口产生脉冲
 * Description: 驱动测试程序
 * Author & Date: Joshua Chan, 2011/12/26
 * ***********************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "qsync_psdr.h"

int main(void)
{
    int i;
    int width = 100;       //脉宽
    int cycle = 1000;       //周期
    int duration = 990000; //持续发送时间, 单位为s
    int fd;

    fd = open(PULSE_DEVICE_PATH, O_RDWR);
    if (fd < 0) {
        perror("open device");
        return -1;
    }

    printf("start: width=%dms, cycle=%dms, duration=%dms\n",
           width, cycle, duration/1000);
    usleep(1);  //延时, 以便应用层与驱动层的打印信息分开

    /************* 调频脉冲发送示例 **************/
    ioctl(fd, PS_FM_WIDTH_SET, width); //设置脉宽
    ioctl(fd, PS_FM_CYCLE_SET, cycle); //设置周期
    ioctl(fd, PS_FM_TYPE_SET, FM_TYPE_HIGHER);  //设置类型
    ioctl(fd, PS_FM_START, 0);         //开始发送
    usleep(duration);

    for (i = 2; i <= 6; i++) {
        /* 在调频脉冲发送期间, 改变脉宽及类型 */
        ioctl(fd, PS_FM_TYPE_SET, (i % 2) ? FM_TYPE_HIGHER : FM_TYPE_LOWER);
        ioctl(fd, PS_FM_WIDTH_SET, width * i);
        usleep(duration);  //持续发送时间
    }

    ioctl(fd, PS_FM_STOP, 0);  //停止发送
    usleep(1);

    width = 0;
    cycle = 0;
    ioctl(fd, PS_FM_WIDTH_GET, &width); //读脉宽
    ioctl(fd, PS_FM_CYCLE_GET, &cycle); //读周期
    printf("read: width=%dms, cycle=%dms\n", width, cycle);

    /************* 合闸脉冲发送示例 **************/
    ioctl(fd, PS_CS_WIDTH_SET, width); //设置脉宽
    ioctl(fd, PS_CS_CYCLE_SET, cycle); //设置周期
    ioctl(fd, PS_CS_START, 0);         //开始发送
    usleep(duration);

    for (i = 2; i <= 6; i++) {
        /* 在合闸脉冲发送期间, 改变脉宽 */
        ioctl(fd, PS_CS_WIDTH_SET, width * i);
        usleep(duration);  //持续发送时间
    }

    ioctl(fd, PS_CS_STOP, 0);  //停止发送
    usleep(1);

    width = 0;
    cycle = 0;
    ioctl(fd, PS_CS_WIDTH_GET, &width); //读脉宽
    ioctl(fd, PS_CS_CYCLE_GET, &cycle); //读周期
    printf("read: width=%dms, cycle=%dms\n", width, cycle);

    usleep(1);
    close(fd);
    return 0;
}