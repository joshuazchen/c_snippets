模拟脉冲宽度测量程序

　　固定频率的脉冲信号上升沿和下降沿之间的时差，即半个周期，测量这个时差，便可得出频率值。本程序通过操作按键来模拟脉冲信号，按键抬起，则触发上升沿中断，按下则触发下降沿中断，检测二次中断期间定时器的计数差值，并据此算出时间差。

　　本程序中定时器中断频率为100赫兹，通过读取定时器中断计数值及定时器监视寄存器（TCNTO）的值，计时精度可达0.2微秒。

　　程序由计时器驱动模块<pwm.c>，按键中断模块<button.c>及测试程序<test.c>组成。