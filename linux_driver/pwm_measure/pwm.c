/* ******************************************************
 * Name: pwm.c
 * Proj: 测量脉冲宽度(Mini2440)
 * Desc: 用按键模拟脉冲源, 计算从按键抬起至下次按键按下所经过的时间
         本模块通过设置定时器, 实现计时功能
 * Author & Date: Joshua Chan, 2011/12/02
 * ******************************************************/
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/interrupt.h>
#include <linux/clk.h>
#include <linux/irq.h>
#include <plat/clock.h>
#include <plat/regs-timer.h>
#include <mach/hardware.h>
#include <mach/regs-gpio.h>
#include <mach/irqs.h>
#include <asm/io.h>

/* 定义定时器发生中断的频率为100 */
#define FREQ 100

/* 定义时器中断计数 */
static volatile unsigned long my_jif;

/* 定时器中断处理函数 */
static irqreturn_t pwm_isr(int irq, void *pin)
{
    my_jif++;
    return IRQ_HANDLED;
}

/* 定时器缓冲寄存器的值, 当中断频率为100时, 其值为50625 */
static unsigned long tcnt;

/* 设置定时器中断频率函数 */
static void pwm_freq_set(unsigned long freq)
{
    unsigned long tcfg0;
    unsigned long tcfg1;
    unsigned long tcon;
    unsigned long tcmp;
    struct clk *pclk;
    unsigned long pclk_rate;

    /* 先通过clk_get()函数取得pclk结构内容, 再用clk_get_rate()
     * 获得pclk的频率 */
    pclk = clk_get(NULL, "pclk");
    pclk_rate = clk_get_rate(pclk);
    printk("Pclk_rate = %lu\n", pclk_rate);

    /* 配置TCFG0寄存器, 设置prescaler值为4 */
    tcfg0 = __raw_readl(S3C2410_TCFG0);
    tcfg0 &= ~S3C2410_TCFG_PRESCALER0_MASK;
    tcfg0 |= (5 - 1);
    __raw_writel(tcfg0, S3C2410_TCFG0);
    printk("Prescaler value = %d\n", 5-1);

    /* 配置TCFG1寄存器, 设置divider值为2 */
    tcfg1 = __raw_readl(S3C2410_TCFG1);
    tcfg1 &= ~S3C2410_TCFG1_MUX0_MASK;
    tcfg1 |= S3C2410_TCFG1_MUX0_DIV2;
    __raw_writel(tcfg1, S3C2410_TCFG1);
    printk("Divider value = %d\n", 2);

    /* 根据freq值求出计数值, 并存入TCNTB0和TCMPB0中 */
    tcnt = (pclk_rate / 5 / 2) / freq;
    tcmp = tcnt / 2;
    __raw_writel(tcnt, S3C2410_TCNTB(0));
    __raw_writel(tcmp, S3C2410_TCMPB(0));
    printk("Timer input clock freq (pclk/5/2) = %lu\n", pclk_rate/5/2);
    printk("tcnt = %lu\n", tcnt);

    /* 配置TCON寄存器, 设置循环加载, 更新计数值并启动计数器 */
    tcon = __raw_readl(S3C2410_TCON);
    tcon &= ~0x1F;
    tcon |= 0xB;
    __raw_writel(tcon, S3C2410_TCON);

    /* 根据设备手册, 将计数值更新位清零, 以便下次写入 */
    tcon &= ~0x2;
    __raw_writel(tcon, S3C2410_TCON);
}

/* 停止定时器 */
static void pwm_stop(void)
{
    unsigned long tcon;

    tcon = __raw_readl(S3C2410_TCON);
    tcon &= ~1;
    __raw_writel(tcon, S3C2410_TCON);
}

/* 定义file_operations结构, 准备混杂设备的注册 */
static struct file_operations pwm_fops = {
    .owner = THIS_MODULE,
};

/* 定义混杂设备结构, 填充必要信息 */
static struct miscdevice pwm_misc = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "my_pwm",
    .fops = &pwm_fops,
};

/* 驱动入口函数, 申请中断, 注册混杂设备, 设置频率 */
static int __init pwm_init(void)
{
    request_irq(IRQ_TIMER0, pwm_isr, IRQ_TYPE_EDGE_BOTH, "my_timer", &tcnt);
    misc_register(&pwm_misc);
    pwm_freq_set(FREQ);

    return 0;
}

/* 驱动出口函数, 完成清理工作 */
static void __exit pwm_exit(void)
{
    free_irq(IRQ_TIMER0, &tcnt);
    pwm_stop();
    misc_deregister(&pwm_misc);
}

module_init(pwm_init);
module_exit(pwm_exit);
MODULE_LICENSE("GPL");

/* 导出变量, 以便在按键中断处理函数中使用 */
EXPORT_SYMBOL(my_jif);
EXPORT_SYMBOL(tcnt);