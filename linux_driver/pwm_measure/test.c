/* ******************************************************
 * Name: test.c
 * Proj: 测量脉冲宽度(Mini2440)
 * Desc: 驱动测试程序
 * Author & Date: Joshua Chan, 2011/12/02
 * ******************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>

int main(void)
{
    int fd;
    unsigned long t[2];
    double time;

    fd = open("/dev/my_button", O_RDONLY);
    if (fd < 0) {
        printf("Open /dev/my_button failed.\n");
        return -1;
    }

    /* 循环读取测量结果, 转换成时间单位打印输出 */
    while (true) {
        read(fd, &t, sizeof(t));
        time = (t[0] / 100.0) + (t[1] / 100.0 / 50625.0);
        printf("================================\n");
        printf("time = %.2lf s\n\n", time);
    }

    close(fd);

    return 0;
}