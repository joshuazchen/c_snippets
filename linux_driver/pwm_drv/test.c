/* ***************************************
 * Name: pwm_test.c
 * Desc: PWM驱动测试程序(Mini2440)
 * Author & Date: Joshua Chan, 2011/12/01
 * **************************************/
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "pwm_drv.h"

/* 测试程序, 根据程序参数对pwm进行设置频率或停止操作 */
int main(int argc, char *argv[])
{
    int fd;
    int arg;

    if (argc != 2 && argc != 3) {
        printf("Usage: %s  <cmd>  [freq]\n", argv[0]);
        printf("cmd: 1 - stop, 2 - set freq\n");
        printf("freq suggested: 20 ~ 20000\n");
        return -1;
    }

    fd = open("/dev/my_pwm", O_WRONLY);
    if (fd < 0) {
        printf("Open /dev/my_pwm failed.\n");
        return -1;
    }

    if (!strcmp(argv[1], "1")) {
        ioctl(fd, IOC_STOP_PWM, arg);
    } else if (!strcmp(argv[1], "2")) {
        arg = atoi(argv[2]);
        printf("arg = %d\n", arg);
        ioctl(fd, IOC_SET_FREQ, arg);
    }

    sleep(3);
    close(fd);

    return 0;
}