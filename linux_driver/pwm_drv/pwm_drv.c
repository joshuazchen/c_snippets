/* ***************************************
 * Name: pwm_drv.c
 * Desc: PWM驱动程序(Mini2440)
 * Author & Date: Joshua Chan, 2011/12/01
 * **************************************/
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/clk.h>
#include <plat/clock.h>
#include <plat/regs-timer.h>
#include <mach/hardware.h>
#include <mach/regs-gpio.h>
#include <asm/io.h>
#include "pwm_drv.h"

/* 为保证设备文件同一时间只被一个程序打开, 定义一个互斥锁 */
static DECLARE_MUTEX(pwm_lock);

/* 设置pwm频率 */
static void pwm_freq_set(unsigned long freq)
{
    unsigned long tcfg0;
    unsigned long tcfg1;
    unsigned long tcon;
    unsigned long tcnt;
    unsigned long tcmp;
    struct clk *pclk;
    unsigned long pclk_rate;

    /* 先通过clk_get()函数取得pclk结构内容, 再用clk_get_rate()
     * 获得pclk的频率 */
    pclk = clk_get(NULL, "pclk");
    pclk_rate = clk_get_rate(pclk);
    printk("Pclk_rate = %lu\n", pclk_rate);

    /* 配置GPB0为TOUT状态, 打开蜂鸣器 */
    s3c2410_gpio_cfgpin(S3C2410_GPB0, S3C2410_GPB0_TOUT0);

    /* 配置TCFG0寄存器, 设置prescaler值为24 */
    tcfg0 = __raw_readl(S3C2410_TCFG0);
    tcfg0 &= ~S3C2410_TCFG_PRESCALER0_MASK;
    tcfg0 |= (25 - 1);
    __raw_writel(tcfg0, S3C2410_TCFG0);

    /* 配置TCFG1寄存器, 设置divider值为1/4 */
    tcfg1 = __raw_readl(S3C2410_TCFG1);
    tcfg1 &= ~S3C2410_TCFG1_MUX0_MASK;
    tcfg1 |= S3C2410_TCFG1_MUX0_DIV4;
    __raw_writel(tcfg1, S3C2410_TCFG1);

    /* 根据freq值求出计数值, 并存入TCNTB0和TCMPB0中 */
    tcnt = (pclk_rate / 25 / 4) / freq;
    tcmp = tcnt / 2;
    __raw_writel(tcnt, S3C2410_TCNTB(0));
    __raw_writel(tcmp, S3C2410_TCMPB(0));

    /* 配置TCON寄存器, 设置循环加载, 更新计数值并启动计数器 */
    tcon = __raw_readl(S3C2410_TCON);
    tcon &= ~0x1F;
    tcon |= 0xB;
    __raw_writel(tcon, S3C2410_TCON);

    /* 根据设备手册, 将计数值更新位清零, 以便下次写入 */
    tcon &= ~0x2;
    __raw_writel(tcon, S3C2410_TCON);
}

/* 配置GPB0为output, 即可停止pwm */
static void pwm_stop(void)
{
    s3c2410_gpio_cfgpin(S3C2410_GPB0, S3C2410_GPB0_OUTP);
    s3c2410_gpio_setpin(S3C2410_GPB0, 0);
}

/* 定义open()函数, 获取互斥锁 */
static int pwm_open(struct inode *inode, struct file *file)
{
    down_killable(&pwm_lock);

    return 0;
}

/* 定义ioctl()方法, 以便可以自由修改频率 */
static int pwm_ioctl(struct inode *inode, struct file *file,
                     unsigned int cmd, unsigned long freq)
{
    /* 判断命令格式是否正确, 命令类型在头文件pwm_drv.h中定义 */
    if (_IOC_TYPE(cmd) != IOC_MAGIC)
        return -EINVAL;
    if (_IOC_NR(cmd) > IOC_MAXNR)
        return -EINVAL;

    printk("cmd = %u, freq = %lu\n", cmd, freq);

    switch (cmd) {
    case IOC_SET_FREQ:
        pwm_freq_set(freq);
        break;
    case IOC_STOP_PWM:
        pwm_stop();
        break;
    }

    return 0;
}

/* 定义close()函数, 释放锁 */
static int pwm_close(struct inode *inode, struct file *file)
{
    pwm_stop();
    up(&pwm_lock);
    
    return 0;
}

/* 定义file_operations结构, 准备混杂设备的注册 */
static struct file_operations pwm_fops = {
    .owner = THIS_MODULE,
    .open = pwm_open,
    .ioctl = pwm_ioctl,
    .release = pwm_close,
};

/* 定义混杂设备结构, 填充必要信息 */
static struct miscdevice pwm_misc = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "my_pwm",
    .fops = &pwm_fops,
};

/* 驱动入口函数, 注册混杂设备 */
static int __init pwm_init(void)
{
    misc_register(&pwm_misc);

    return 0;
}

/* 驱动出口函数, 注销混杂设备 */
static void __exit pwm_exit(void)
{
    misc_deregister(&pwm_misc);
}

module_init(pwm_init);
module_exit(pwm_exit);
MODULE_LICENSE("GPL");