/* ***************************************
 * Name: pwm_drv.h
 * Desc: PWM驱动程序(Mini2440)
 * Author & Date: Joshua Chan, 2011/12/01
 * **************************************/
#ifndef _PWM_DRV_H
#define _PWM_DRV_H

#include <asm-generic/ioctl.h>

/* 定义幻数 */
#define IOC_MAGIC 'k'

/* 定义ioctl方法的命令格式 */
#define IOC_STOP_PWM _IO(IOC_MAGIC, 1)
#define IOC_SET_FREQ _IOR(IOC_MAGIC, 2, int)

/* 命令数量 */
#define IOC_MAXNR    2

#endif