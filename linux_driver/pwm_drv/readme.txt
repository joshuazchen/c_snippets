实现PWM（脉冲宽度调制）驱动程序

根据传入的频率值配置定时器，并打开定时器输出引脚，使蜂鸣器发声。定时器的频率传入和配置由IOCTL方法实现，用混杂设备的方法注册驱动及生成设备文件，并在设备打开时获取互斥锁，禁止对设备文件的并行访问。

代码由三个文件组成，驱动头文件<pwm_drv.h>，驱动程序<pwm_drv.c>，测试程序<pwm_test.c>。