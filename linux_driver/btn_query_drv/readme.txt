查询方式实现简单按键驱动程序

根据《Linux简单字符设备驱动编写方法》一文的步骤，用查询方式简单实现Mini2440开发板的按键驱动程序。

代码共分二个文件，驱动程序<btn_drv.c>和测试程序<btn_test.c>。