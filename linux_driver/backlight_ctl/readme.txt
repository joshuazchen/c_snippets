背光控制程序

　　根据Tiny6410厂家提供的内核源码中的触屏驱动（路径：drivers/input/touchscreen/mini6410_1wire_host.c），背光由设备</dev/backlight-1wire>控制，该设备提供了write方法，根据用户传入的亮度值（0 ~ 127）来设置背光。

　　程序由操作函数<backlight_ctl.c> ，头文件<backlight_ctl.h>，测试程序<test.c>组成。