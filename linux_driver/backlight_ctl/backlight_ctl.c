/* ******************************************
 * File name: backlight_ctl.c
 * Function:  控制tiny6410背光亮度
 * Description: 应用驱动提供的接口控制背光的亮度
 * Author & Date: Joshua Chan, 2011/12/31
 * ******************************************/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include "backlight_ctl.h"

static int fd_backlight_1wire;

int set_backlight_brightness(unsigned bright)
{
    if (!fd_backlight_1wire) {
        fd_backlight_1wire = open(BACKLIGHT_PATH, O_WRONLY);
        if (fd_backlight_1wire < 0) {
            perror("open backlight device:");
            return -1;
        }
    }

    char buf[4] = {0, 0, 0, 0};
    sprintf(buf, "%u", bright);

    int ret = write(fd_backlight_1wire, buf, sizeof(buf));

    return ret;
}