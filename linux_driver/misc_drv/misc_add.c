/* ***************************************
 * Name: misc_add.c
 * Desc: Linux混杂设备驱动编写方法
 * Author & Date: Joshua Chan, 2011/11/30
 * **************************************/
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>

/* 为file_operations结构体准备open函数 */
int myopen(struct inode *inode, struct file *file)
{
    printk("My device opened.\n");
    return 0;
}

/* 为file_operations结构体准备write函数 */
ssize_t mywrite(struct file *file, const char __user *buf, size_t size, loff_t *l)
{
    printk("My device wrote.\n");
    return 0;
}

/* 为miscdevice结构作准备, 定义file_operations结构体, 填充操作函数 */
struct file_operations myfops = {
    .owner = THIS_MODULE,
    .open = myopen,
    .write = mywrite,
};

/* 定义混杂设备结构, 主要设置设备名和设备文件操作方法 */
struct miscdevice my_misc = {
    .minor = MISC_DYNAMIC_MINOR, // 此宏表示由内核分配次设备号
    .name = "my_misc",           // 设备名
    .fops = &myfops,             // 设备操作方法
};

/* 驱动入口函数 */
int __init myinit(void)
{
    /* 调用misc_register()函数, 根据定义的miscdevice结构注册混杂设备,
     * 系统将自动完成包括驱动注册与设备文件生成等一系列工作 */
    misc_register(&my_misc);

    return 0;
}

/* 驱动出口函数 */
void __exit myexit(void)
{
    /* 完成设备与驱动注销工作 */
    misc_deregister(&my_misc);
}

module_init(myinit);
module_exit(myexit);

MODULE_LICENSE("GPL");