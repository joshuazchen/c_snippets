/* ***************************************
 * Name: misc_test.c
 * Desc: Linux���ַ��豸�������Գ���
 * Author & Date: Joshua Chan, 2011/11/30
 * **************************************/
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main(void)
{
    char mypath[] = "/dev/my_misc";
    int fd = open(mypath, O_RDWR);

    if (fd < 0) {
        printf("Open %s failure.\n", mypath);
        return -1;
    }
    write(fd, "hello", 6);

    return 0;
}