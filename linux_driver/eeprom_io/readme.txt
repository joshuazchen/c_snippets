Linux下的eeprom读写操作

　　利用Linux内核自带的IIC总线驱动，按系统提供的框架，用ioctl方法对eeprom设备进行读写操作，并分别按单字节及多字节读写方式，对外提供函数接口，以供外部程序方便调用。
　　程序由3个文件组成，分别为头文件<eeprom_io.h>，函数实现文件<eeprom_io.c>，及测试程序<test.c>。