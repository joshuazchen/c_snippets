/* ***************************************
 * File Name: test.c
 * Function:  实现日志记录功能
 * Description: 测试程序
 * Author & Date: Joshua Chan, 2012/01/01
 * ***************************************/
#include <stdio.h>
#include <errno.h>
#include "jlog.h"

int main()
{
    /* 设置日志模式标志 */
    jlog_mode = JLOG_TO_FILE_CONSOLE;

    /* 生成一个错误代码 */
    FILE *fp = fopen("not_exist", "r");

    /* 生成日志示例 */
    JLOG("%s: %m", "fopen", errno);
    JLOG(J_FATAL"test %d", 2);
    JLOG("<1>test%d", 3);

    return 0;
}