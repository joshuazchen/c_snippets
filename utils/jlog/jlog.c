/* ***************************************************************
 * File Name: jlog.c
 * Function:  实现日志记录功能
 * Description: 主要功能:
 *              1. 支持在信息的开始位置指定信息级别, 未指定则会认为是默认级别
 *              2. 支持四种模式:
 *                 日志关闭/写入文件/控制台显示/同时写入文件和控制台显示
 * Author & Date: Joshua Chan, 2012/01/01
 * ***************************************************************/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include "jlog.h"

int jlog_mode;

/* 信息标志文字, 根据信息级别加入进日志文件 */
static char *logflag[] = {
    FLAG_FATAL,
    FLAG_ERR,
    FLAG_WARN,
    FLAG_INFO,
    FLAG_DEBUG,
};

/* 以"yy/mm/dd hh:mm:ss"的格式获取当前时间 */
static inline char *get_time(void)
{
    static char sys_time[64];
    time_t t = time(NULL);
    struct tm *tmp = localtime(&t);

    memset(sys_time, 0, sizeof(sys_time));
    sprintf(sys_time, "[%d/%02d/%02d %02d:%02d:%02d]",
            tmp->tm_year + 1900,
            tmp->tm_mon + 1,
            tmp->tm_mday,
            tmp->tm_hour,
            tmp->tm_min,
            tmp->tm_sec);

    return sys_time;
}

/* 将参数地址中的字符串头尾去空白 */
static inline char *str_trim(char *s)
{
    int i = 0;
    int j = 0;
    int k;
    int len = strlen(s);
    char *tmp = s;
    
    while ((tmp[i] == ' ') || (tmp[i] == '\t') || (tmp[i] == '\n'))
        i++;
    if (i == len) {
        s[0] = '\0';
    } else {
        while ((tmp[len-1-j] == ' ') || (tmp[len-1-j] == '\t')
               || (tmp[len-1-j] == '\n'))
            j++;
        for (k = 0; k < len-i-j; k++)
            s[k] = s[k+i];
        s[k] = '\0';
    }

    return s;
}

/* 从参数地址的字符串中获取信息的级别 */
static inline int get_prior(char *str)
{
    int ret;

    if (!strncmp(str, J_FATAL, 3))
        ret = INDEX_FATAL;
    else if (!strncmp(str, J_ERR, 3))
        ret = INDEX_ERR;
    else if (!strncmp(str, J_WARN, 3))
        ret = INDEX_WARN;
    else if (!strncmp(str, J_INFO, 3))
        ret = INDEX_INFO;
    else if (!strncmp(str, J_DEBUG, 3))
        ret = INDEX_DEBUG;
    else
        ret = INDEX_NONE;

    return ret;
}

/* 根据信息级别获取标志文字, 存入参数地址 */
static inline char *get_logflag(int prior)
{
    return logflag[prior];
}

static FILE *jlogfp;

/* 打开日志文件 */
static inline int open_logfile(void)
{
    jlogfp = fopen(LOGFILE_PATH, "a");
    if (!jlogfp) {
        perror("fopen");
        return -1;
    }
    return 0;
}

/* 将信息存入日志文件
   传入参数的开始位置可以指定信息级别, 未指定级别将转为默认级别
   例如: jlog("%m", errno); 将出错信息以默认级别写入日志文件
   函数返回值为实际写入的字符数
 */
int jlog(char *fmt, ...)
{
    int ret = 0;

    /* 日志文件在第一次写入时会自动打开 */
    if (jlog_mode == JLOG_OFF)
        return 0;
    else if (!jlogfp && (jlog_mode == JLOG_TO_FILE
                         || jlog_mode == JLOG_TO_FILE_CONSOLE)) {
        if (ret = open_logfile())
            return -1;
    }

    char str[256];
    /* 拷贝参数字符串, 以便去除首尾空白及信息级别标识 */
    memset(str, 0, sizeof(str));
    sprintf(str, "%s", fmt);

    char *tmp = str_trim(str);  //去除首尾空白
    int prior = get_prior(tmp); //获取信息级别
    if (prior != INDEX_NONE)
        tmp += 3;               //去除开头的信息级别标识

    if (prior == INDEX_NONE)    //无级别标识将认为是默认级别
        prior = INDEX_DEFAULT;

    va_list args;
    va_start(args, fmt);

    /* 信息写入文件 */
    if (jlog_mode == JLOG_TO_FILE
        || jlog_mode == JLOG_TO_FILE_CONSOLE) {
        fprintf(jlogfp, "%s%s", get_time(), get_logflag(prior));
        ret = vfprintf(jlogfp, tmp, args);
        fprintf(jlogfp, "\n");
        fflush(jlogfp);
    }

    /* 信息在控制台显示 */
    if (jlog_mode == JLOG_TO_CONSOLE
               || jlog_mode == JLOG_TO_FILE_CONSOLE) {
        ret = vprintf(tmp, args);
        printf("\n");
    }

    va_end(args);

    return ret;
}