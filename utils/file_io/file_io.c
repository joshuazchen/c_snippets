/* ********************************************
 * Name: file_io.c
 * Desc: 对文件整行读写的封装, 可以简化文件操作
 *       示例代码请见<简单信息管理系统>
 * Author & Date: Joshua Chan, 2011/11/08
 * *******************************************/
#include <stdio.h>
#include "main.h"

static FILE *fd;

/* 只写方式打开    */
bool file_open_r(char *path)
{
    fd = fopen(path, "r");
    return (fd == NULL) ? false : true;
}

/* 只读方式打开    */
bool file_open_w(char *path)
{
    fd = fopen(path, "w");
    return (fd == NULL) ? false : true;
}

/* 关闭文件        */
int file_close(void)
{
    return fclose(fd);
}

/* 从文件中读取一行, 并将内容返回    */
char *read_line(void)
{
    return fgets(s, MAX_LEN - 1, fd);
}

/* 向文件中写入一行        */
int write_line(char *s)
{
    return fprintf(fd, "%s\n", s);
}