/* **************************************
 * Name: bubble.c
 * Description: 实现冒泡排序
 * Author & Date: Joshua Chan 2011/11/11
 * *************************************/
#include <stdbool.h>

void bubble_sort(int *array, int size)
{
    int i, j;
    int tmp;
    bool flag;    /* 设置标志, 一次排序未发现数据交换, 则结束    */

    for (i = size - 1; i >= 0; i--) {
        flag = true;
        for (j = 0; j != i; j++) {
            if (array[j] > array[j+1]) {
                tmp = array[j];
                array[j] = array[j+1];
                array[j+1] = tmp;
                flag = false;
            }
        }
        if (flag)
            break;
    }
}