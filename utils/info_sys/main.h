/* *******************************************************
 * Name: main.c
 * Proj: 简单信息管理系统
 * Desc: 包括数据操作有: 增加/遍历/排序/查询及删除匹配数据
 *        /数据文件的导入和导出
 * Author & Date: Joshua Chan, 2011/11/08
 * ******************************************************/
#ifndef _MAIN_H
#define _MAIN_H
#include <stdbool.h>
#include "list.h"

#define MAX_LEN 81              /* 预设最大字符串长度    */
#define PATH "./student.dat"    /* 数据文件路径          */

/* 定义数据结构        */
struct student {
    int id;
    char name[20];
    char gender[10];
    char depart[20];
};

/* util.c文件中有关字符串处理的接口声明      */
char s[MAX_LEN];
inline char *get_str(void);
int split_num(char *s, const char *delim);
char *str_trim(char *s);

/* menu.c文件中有关界面操作的接口声明        */
void disp_menu(void);
void select_menu(LIST l);

/* operate.c文件中有关数据操作的接口        */
void add_student(LIST l);
void query_student(LIST l);
void sort_student(LIST l);
void search_student(LIST l);
void remove_student(LIST l);
bool import_data(LIST l, char *path);
bool export_data(LIST l, char *path);
bool save_manual(LIST l);

/* file_io.c中封装的文件操作接口            */
bool file_open(char *path);
int file_close(void);
char *read_line(void);
int write_line(char *s);

#endif