#ifndef _PROTOCOL_H
#define _PROTOCOL_H

#include "macro.h"
#include "queue.h"
#include "command.h"

/* 协议格式如下:
 * [STX1][STX2][command1][command2][length][data ...][checksum][ETX1][EXT2]
 *
 * STX: 起始标志
 * command: 命令
 * length: 不定长数据的长度
 * data: 不定长数据, 长度受PACK_MAX_LENGTH限制
 * checksum: command1, command2, length, data 四部份检验值
 * ETX: 结束标志
 */
#define STX1 '{'                /* 起始标志1 */
#define STX2 '<'                /* 起始标志2 */
#define ETX1 '>'                /* 结束标志1 */
#define ETX2 '}'                /* 结束标志2 */

#define PACK_MAX_LENGTH  40     /* 接收数据最大长度 */
#define PACK_HEAD_LENGTH  5     /* 包头长度 */
#define PACK_TAIL_LENGTH  3     /* 包尾长度 */

#define PACK_STX1_OFFSET     0  /* STX1相对包起始位置的偏移 */
#define PACK_STX2_OFFSET     1  /* STX2相对包起始位置的偏移 */
#define PACK_COMMAND1_OFFSET 2  /* command1相对包起始位置的偏移 */
#define PACK_COMMAND2_OFFSET 3  /* command2相对包起始位置的偏移 */
#define PACK_LENGTH_OFFSET   4  /* length相对包起始位置的偏移 */
#define PACK_DATA_OFFSET     5  /* data相对包起始位置的偏移 */
#define PACK_CHECKSUM_OFFSET 5  /* checksum相对包起始位置的偏移减data段长度 */
#define PACK_ETX1_OFFSET     6  /* EXT1相对包起始位置的偏移减data段长度 */
#define PACK_ETX2_OFFSET     7  /* EXT2相对包起始位置的偏移减data段长度 */

/* 按协议格式发送数据包, 参数中传入command, length, data的值 */
void pack_send(cmd1 command1, cmd2 command2, u8 length, const void *data);

/* 解析传入的队列, 要求传入的队列中含有'STX'及'ETX'标志 */
bool pack_decode(queue_st *q);

/* 获取最后接收数据包的command1值 */
u8 pack_get_command1(void);

/* 获取最后接收数据包的command2值 */
u8 pack_get_command2(void);

/* 获取最后接收数据包的length值 */
u8 pack_get_length(void);

/* 获取最后接收数据包的data数据 */
u8 *pack_get_data(void);

/* 获取最后接收数据包的checksum值 */
u8 pack_get_rx_checksum(void);

#endif
