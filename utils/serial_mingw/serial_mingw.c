#include <stdio.h>
#include <stdbool.h>
#include <windows.h>

/* HANDLE definition is: typedef void *PVOID; typedef PVOID HANDLE;
 * DWORD definition is: typedef unsigned long DWORD;
 */

/* Opening */
HANDLE serial_open(void)
{
	LPCSTR portname = "COM1";
	DWORD accessDirection = GENERIC_READ | GENERIC_WRITE;
	HANDLE hSerial = CreateFile(portname,
								accessDirection,
								0,
								0,
								OPEN_EXISTING,
								0,
								0);
	if (hSerial == INVALID_HANDLE_VALUE) {
		printf("Open failed\n");
		return NULL;
	}

	return hSerial;
}

/* Basic sets */
bool serial_basic_set(HANDLE hSerial)
{
	DCB dcbSerialParams = {0};
	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	if (!GetCommState(hSerial, &dcbSerialParams)) {
		printf("Get state failed\n");
	}
	dcbSerialParams.BaudRate = 38400;
	dcbSerialParams.ByteSize = 8;
	dcbSerialParams.StopBits = ONESTOPBIT;
	dcbSerialParams.Parity = NOPARITY;
	if (!SetCommState(hSerial, &dcbSerialParams)) {
		printf("Set state failed\n");
		return -1;
	}

	return 0;
}

/* Timeouts need to be set so that the program does not hang up
 * when receiving nothing.
 */
bool serial_timeout_set(HANDLE hSerial)
{
	COMMTIMEOUTS timeouts = {0};
	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutConstant = 50;
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = 50;
	timeouts.WriteTotalTimeoutMultiplier = 10;
	if (!SetCommTimeouts(hSerial, &timeouts)) {
		printf("Set timeout failed\n");
		return -1;
	}

	return 0;
}

/* Reading */
DWORD readFromSerialPort(HANDLE hSerial, unsigned char *buffer, int buffersize)
{
	DWORD dwBytesRead = 0;
	if (!ReadFile(hSerial, buffer, buffersize, &dwBytesRead, NULL)) {
		printf("Read failed\n");
	}

	return dwBytesRead;
}

/* Writing */
DWORD writeToSerialPort(HANDLE hSerial, unsigned char *data, int length)
{
	DWORD dwBytesRead = 0;
	if (!WriteFile(hSerial, data, length, &dwBytesRead, NULL)) {
		printf("Write failed\n");
	}

	return dwBytesRead;
}

/* Closing */
void closeSerialPort(HANDLE hSerial)
{
	CloseHandle(hSerial);
}
