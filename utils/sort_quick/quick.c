/* **************************************
 * Name: quick.c
 * Description: 实现快速排序
 * Author & Date: Joshua Chan 2011/11/12
 * **************************************/
inline void swap(int *a, int *b);
int partition(int *array, int first, int last);

/* 排序函数, 为了便于理解, 将分区部分独立出来 */
void quick_sort(int *array, int first, int last)
{
    int i;

    if (first < last) {
        i = partition(array, first, last);
        quick_sort(array, first, i-1);
        quick_sort(array, i+1, last);
    }
}

/* 数组分区, 先选定一个中间值
 * 将比中间值小的排前面, 比中间值大的排后面 */
int partition(int *array, int first, int last)
{
    int key = array[first];

    while (first < last) {
        for (; first < last && array[last] >= key; last--)
            ;
        swap(&array[first], &array[last]);

        for (; first < last && array[first] <= key; first++)
            ;
        swap(&array[first], &array[last]);
    }

    return first;
}

inline void swap(int *a, int *b)
{
    int tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}