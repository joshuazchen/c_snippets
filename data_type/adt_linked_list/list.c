/* ***********************************************************************
 * Name: link.c
 * Desc: C语言ADT(抽象数据类型)的实现
 *       本结构为线性表, 以链式存储方式组织
 *       实现的操作有: 
 *       结构初始化/销毁与空间释放/向头部或尾部加入记录/在指定位置加入记录
 *       /自动在有序表中寻找合适位置加入记录/删除指定位置的记录
 *       /删除符合条件的记录/读取指定位置记录/查找符合条件的记录
 *       /按给定的条件排序(可自定义排序依据的字段及排序方向)
 * Author: Joshua Chan
 * Date: 2011-11-03
 * ***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "link.h"

/* 判断要获取的位置是否有效                    */
static inline bool getpos_invalid(LIST_T ptr, const int pos)
{
    return (pos > (((struct list_st *)ptr)->elmnr -1) || pos < 0) ? true : false;
}

/* 判断要加入记录的位置是否有效                */
static inline bool putpos_invalid(LIST_T ptr, const int pos)
{
    return (pos > (((struct list_st *)ptr)->elmnr) || pos < 0) ? true : false;
}

/* 判断记录非空                                */
static inline bool list_empty(LIST_T ptr)
{
    return ((struct list_st *)ptr)->elmnr == 0 ? true : false;
}

/* 结构初始化                                */
LIST_T list_init(const int elmsize)
{
    struct list_st *newlist;

    newlist = malloc(sizeof(struct list_st));
    if (newlist == NULL)
        return NULL;

    newlist->elmsize = elmsize;
    newlist->elmnr = 0;

    newlist->head.datap = NULL;
    newlist->head.prev = &newlist->head;
    newlist->head.next = &newlist->head;

    return (LIST_T)newlist;
}

/* 结构销毁, 空间释放                        */
int list_release(LIST_T ptr)
{
    struct list_st *me = ptr;
    struct node_st *cur, *save;

    for (cur = me->head.next; cur != &me->head; cur = save) {
        save = cur->next;
        free(cur->datap);
        free(cur);
    }

    free(me);

    return 0;
}

/* 预分配空间                                */
static struct node_st *node_alloc(LIST_T ptr, void *datap)
{
    struct list_st *me = ptr;
    struct node_st *newnode;

    newnode = malloc(sizeof(struct node_st));
    if (newnode == NULL)
        return NULL;

    newnode->datap = malloc(me->elmsize);
    if (newnode->datap == NULL) {
        free(newnode);
        return NULL;
    }

    memcpy(newnode->datap, datap, me->elmsize);

    return newnode;
}

/* 根据给定条件选择合适位置加入记录, 适用于有序表    */
int list_putif(LIST_T ptr, comp_func_t comp, void *datap, const bool ord)
{
    struct list_st *me = ptr;
    struct node_st *cur, *newnode;
    bool sig;

    if (list_empty(me))
        return -1;

    newnode = node_alloc(ptr, datap);
    if (newnode == NULL)
        return -1;

    for (cur = me->head.next; cur != &me->head; cur = cur->next) {
        sig = comp(cur->datap, datap) > 0 ? true : false;
        if (!(sig ^ ord))
            break;
    }

    newnode->prev = cur->prev;
    newnode->next = cur;
    cur->prev->next = newnode;
    cur->prev = newnode;
    me->elmnr++;

    return 0;
}

/* 在给定位置加入记录                        */
int list_putpos(LIST_T ptr, void *datap, const int pos)
{
    struct list_st *me = ptr;
    struct node_st *cur, *newnode;
    int i;

    if (putpos_invalid(me, pos))
        return -1;

    newnode = node_alloc(ptr, datap);
    if (newnode == NULL)
        return -1;

    for (cur = me->head.next, i = 0; i < pos; cur = cur->next, i++)
        ;

    newnode->prev = cur->prev;
    newnode->next = cur;
    cur->prev->next = newnode;
    cur->prev = newnode;
    me->elmnr++;

    return 0;
}

/* 追加记录                                    */
int list_append(LIST_T ptr, void *datap)
{
    struct list_st *me = ptr;
    struct node_st *newnode;

    newnode = node_alloc(ptr, datap);
    if (newnode == NULL)
        return -1;

    newnode->prev = me->head.prev;
    newnode->next = &me->head;
    me->head.prev->next = newnode;
    me->head.prev = newnode;
    me->elmnr++;

    return 0;
}

/* 头部加入记录                                */
int list_prepend(LIST_T ptr, void *datap)
{
    struct list_st *me = ptr;
    struct node_st *newnode;

    newnode = node_alloc(ptr, datap);
    if (newnode == NULL)
        return -1;

    newnode->prev = &me->head;
    newnode->next = me->head.next;
    me->head.next->prev = newnode;
    me->head.next = newnode;
    me->elmnr++;

    return 0;
}

/* 删除指定位置记录                            */
int list_delete_pos(LIST_T ptr, const int pos)
{
    struct list_st *me = ptr;
    struct node_st *cur;
    int i;

    if (list_empty(me) || getpos_invalid(me, pos))
        return -1;

    for (cur = me->head.next, i = 0; i < pos; cur = cur->next, i++)
        ;

    cur->prev->next = cur->next;
    cur->next->prev = cur->prev;
    free(cur->datap);
    free(cur);
    me->elmnr--;

    return 0;
}

/* 删除符合指定条件的记录                    */
int list_delete(LIST_T ptr, comp_func_t comp, void *key)
{
    struct list_st *me = ptr;
    struct node_st *cur;

    if (list_empty(me))
        return -1;

    for (cur = me->head.next; cur != &me->head; cur = cur->next)
        if (comp(cur->datap, key) == 0) {
            cur->prev->next = cur->next;
            cur->next->prev = cur->prev;
            free(cur->datap);
            free(cur);
            me->elmnr--;
            return 0;
        }

    return -1;
}

/* 遍历执行指定操作                            */
int list_travel(LIST_T ptr, proc_func_t proc)
{
    struct list_st *me = ptr;
    struct node_st *cur;

    if (list_empty(me))
        return -1;

    for (cur = me->head.next; cur != &me->head; cur = cur->next)
        proc(cur->datap);

    return 0;
}

/* 取出指定位置记录                            */
void *list_getpos(LIST_T ptr, const int pos)
{
    struct list_st *me = ptr;
    struct node_st *cur;
    int i;

    if (list_empty(me) || getpos_invalid(me, pos))
        return NULL;

    for (cur = me->head.next, i = 0; i < pos; cur = cur->next, i++)
        ;

    return cur->datap;
}

/* 查找符合指定条件的记录                    */
void *list_find(LIST_T ptr, comp_func_t comp, void *key)
{
    struct list_st *me = ptr;
    struct node_st *cur;

    if (list_empty(me))
        return NULL;

    for (cur = me->head.next; cur != &me->head; cur = cur->next)
        if (comp(cur->datap, key) == 0)
            return cur->datap;

    return NULL;
}

/* 以起泡法按指定条件排序                    */
int list_sort(LIST_T ptr, comp_func_t comp, const bool ord)
{
    struct list_st *me = ptr;
    struct node_st *cur, *loop;
    void *tmp;
    bool sig, flag;

    if (list_empty(me))
        return -1;

    for (loop = me->head.prev; loop != &me->head; loop = loop->prev) {
        flag = true;
        for (cur = me->head.next; cur != loop; cur = cur->next) {
            sig = comp(cur->datap, cur->next->datap) > 0 ? true : false;
            if (!(sig ^ ord)) {
                tmp = cur->datap;
                cur->datap = cur->next->datap;
                cur->next->datap = tmp;
                flag = false;
            }
        }
        if (flag == true)
            break;
    }

    return 0;
}