/* *************************************
 * Name: test.c
 * Desc: 双向链式存储线性表操作测试程序
 * Author: Joshua Chan
 * Date: 2011-11-1
 * ************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "double.h"

/* 排序比较函数                    */
int cmp_sort(void *a, void *b)
{
    if (*(int *)a > *(int *)b)
        return 1;
    else if (*(int *)a < *(int *)b)
        return -1;
    else
        return 0;
}

/* 查找比较函数                    */
int cmp_find(struct node *n, void *key)
{
    return (n->data == *(data_t *)key) ? 0 : -1;
}

void put_data(data_t *data, int n)
{
    *data = n;
}

int main(void)
{
    struct link l;
    struct node *n;
    data_t data;
    int i;

    /* 链表初始化            */
    link_init(&l);
    srand(time(NULL));
    /* 填充随机数据            */
    for (i = 0; i < 9; i++) {
        put_data(&data, rand() % 10);
        link_add(&l, &data);
    }
    link_print(&l);

    i = 3;
    /* 查找指定数据并删除相应结点    */
    if ((n = link_find(&l, cmp_find, &i)) != NULL) {
        link_del(&l, n);
        printf("%d was found in data and deleted\n", i);
        link_print(&l);
    } else
        printf("%d was not found in data\n", i);

    /* 按升序排序数据                */
    link_sort(&l, cmp_sort);
    printf("the data was sorted\n");
    link_print(&l);

    link_release(&l);
    return 0;
}