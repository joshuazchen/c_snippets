/* ***************************************************
 * Name: double.h
 * Desc: 双向链式存储线性表的相关操作实现, 包含:
 *       初始化/销毁及空间释放/增加/查找/删除/换位/排序
 * Author: Joshua Chan
 * Date: 2011-11-1
 * **************************************************/
#ifndef _DOUBLE_H_
#define _DOUBLE_H_

#include <stdbool.h>

typedef int data_t;     /* 用户自定义数据类型        */

struct node {           /* 链表结点                  */
    data_t data;        /* 数据存储区域              */
    struct node *prev;
    struct node *next;
};

struct link {
    struct node head;   /* 存储链表头                */
    int nsize;          /* 单个链表结构的大小        */
    int num;            /* 现在链表结点数量          */
};

void link_init(struct link *l);
void link_release(struct link *l);
int link_add(struct link *l, data_t *data);
void link_del(struct link *l, struct node *n);
struct node *link_find(struct link *l, int (*cmp)(struct node *n, void *key), void *key);
void link_print(struct link *l);
void link_sort(struct link *l, int (*cmp)(void *a, void *b));

#endif