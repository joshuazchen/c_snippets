/* *******************************************
 * Name: test.c
 * Desc: 顺序栈的操作测试程序
 * Author: Joshua Chan
 * Date: 2011-11-4
 * *******************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "stack.h"

int main(void)
{
    struct stack s;
    data_t data;
    int i;

    /* 结构初始化                */
    stack_init(&s);
    srand(time(NULL));
    /* 将9个随机数入栈            */
    printf("Put 9 records: ");
    for (i = 0; i < 9; i++) {
        put_data(&data, rand() % 10);
        stack_push(&s, &data);
        print_data(&data);
    }
    putchar('\n');
    printf("Display record: ");
    stack_print(&s);

    /* 将3个数据出栈            */
    printf("Pop 3 records: ");
    for (i = 0; i < 3; i++) {
        stack_pop(&s, &data);
        print_data(&data);
    }
    putchar('\n');
    printf("Display record: ");
    stack_print(&s);

    return 0;
}