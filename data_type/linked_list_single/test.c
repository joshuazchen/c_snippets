/* ***************************************************
 * Name: test.c
 * Desc: 单向链式存储线性表相关操作的测试程序
 * Author: Joshua Chan
 * Date: 2011-11-1
 * **************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "single.h"

/* 查找比较函数                        */
int cmp_find(struct node *n, void *key)
{
    return (n->data == *(data_t *)key) ? 0 : -1;
}

/* 排序比较函数                        */
int cmp_sort(void *a, void *b)
{
    if (*(data_t *)a > *(data_t *)b)
        return 1;
    else if (*(data_t *)a < *(data_t *)b)
        return -1;
    else
        return 0;
}

void put_data(data_t *data, int n)
{
    *data = n;
}

int main(void)
{
    int i;
    struct link l;
    struct node *n;
    data_t data;

    /* 单链表初始化                */
    link_init(&l);
    srand(time(NULL));
    /* 以随机数填充                */
    for (i = 0; i < 10; i++) {
        put_data(&data, rand() % 10);
        link_add(&l, &data);
    }
    link_print(&l);

    i = 3;
    /* 查找给定数据并删除相应结点    */
    if ((n = link_find(&l, cmp_find, &i)) != NULL) {
        //link_del(&l, n);
        link_del(&l, n);
        printf("%d was found and deleted\n", i);
        link_print(&l);
    } else
        printf("%d was not found\n", i);

    /* 对链表排序                    */
    link_sort(&l, cmp_sort);
    printf("data was sorted\n");
    link_print(&l);

    link_release(&l);
    return 0;
}