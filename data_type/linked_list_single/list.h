/* ***************************************************
 * Name: single.h
 * Desc: 单向链式存储线性表相关操作的实现, 包括:
 *       初始化/空间释放/增加结点/查找/删除/遍历打印/排序
 * Author: Joshua Chan
 * Date: 2011-11-1
 * **************************************************/
#ifndef _SINGLE_H_
#define _SINGLE_H_

#include <stdbool.h>

typedef int data_t;     /* 用户自定义数据类型        */

struct node {           /* 单链表结点                */
    data_t data;        /* 数据存储区域              */
    struct node *next;  /* 后继链表指针              */
};

struct link {
    struct node head;   /* 链表头                    */
    int nsize;          /* 链表结点大小              */
    int num;            /* 记录当前结点数量          */
};

void link_init(struct link *l);
void link_release(struct link *l);
int link_add(struct link *l, data_t *data);
struct node *link_prev(struct link *l, struct node *n);
struct node *link_find(struct link *l, int cmp(struct node *n, void *key), void *key);
void link_del(struct link *l, struct node *n);
void link_qdel(struct link *l, struct node *n);
void link_print(struct link *l);
void link_sort(struct link *l, int cmp(void *a, void *b));

#endif