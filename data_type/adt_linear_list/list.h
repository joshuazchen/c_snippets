/* ************************************************
 * Name: list.h
 * Desc: ADT(抽象数据类型)线性存储C语言描述
 *       实现的操作:
 *       初始化/销毁/指定位置追加/查找符合条件记录
 *        /指定位置删除/有序表中自动寻找匹配位置追加
 *        /指定位置读取/按给定条件排序等
 * Author & Date: Joshua Chan, 2011/11/17
 * ************************************************/ 
#ifndef _LIST_H_
#define _LIST_H_
#include <stdbool.h>

#define INIT_SIZE 4        /* 指针数组初始化长度        */
#define STEP_SIZE 2        /* 指针数组扩展步长          */

typedef int (*comp_func)(void *, void *);    /* 比较函数原型        */
typedef void (*proc_func)(void *);           /* 方法函数原型        */

/* 结构定义        */
typedef struct list {
    void **data;    /* 指针数组              */
    int elemsize;   /* 用户数据结构大小      */
    int count;      /* 已存储元素计数        */
    int loads;      /* 最大容量              */
} *LIST;

/* 操作接口        */
LIST list_init(int elemsize);
bool list_release(LIST l);
bool list_addat(LIST l, int index, void *data);
bool list_addlast(LIST l, void *data);
bool list_addfirst(LIST l, void *data);
bool list_addmatch(LIST l, comp_func comp, void *data, bool ord);
bool list_removeat(LIST l, int index);
bool list_removelast(LIST l);
bool list_removefirst(LIST l);
bool list_removematch(LIST l, comp_func comp, void *data);
bool list_travel(LIST l, proc_func proc);
void *list_getat(LIST l, int index);
void *list_getmatch(LIST l, comp_func comp, void *data);
bool list_sort(LIST l, comp_func comp, bool ord);

#endif