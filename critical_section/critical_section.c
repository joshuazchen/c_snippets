#include <stdio.h>
#include <windows.h>
#include <pthread.h>

static int n;
CRITICAL_SECTION cs;

static void* add1(void* arg)
{
	int i;

	for (i = 0; i < 5; i++) {
		EnterCriticalSection(&cs);
		++n;
		printf("add1 ++n\n");
		Sleep(500);
		printf("add1 print n: %d\n", n);
		LeaveCriticalSection(&cs);
		Sleep(500);
	}
	return 0;
}

static void* add2(void* arg)
{
	int i;

	for (i = 0; i < 5; i++) {
		EnterCriticalSection(&cs);
		++n;
		printf("add2 ++n\n");
		Sleep(100);
		printf("add2 print n: %d\n", n);
		LeaveCriticalSection(&cs);
		Sleep(100);
	}
	return 0;
}
int main(void)
{
	pthread_t pt1, pt2;

	InitializeCriticalSection(&cs);
	pthread_create(&pt1, NULL, add1, NULL);
	pthread_create(&pt2, NULL, add2, NULL);
	Sleep(11000);

	return 0;
}
