#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <windows.h>

#define LOGDIR  "d:/log"
#define LOGFILE "a.txt"

int main(void)
{
	FILE *fp;
	char filename[128];
	char info[] = "Debug info.\n";

	if (!access(LOGDIR)) {
		printf("Log dir exist.\n");
	} else {
		printf("Log dir not exist, create dir.\n");
		mkdir(LOGDIR);
	}
	
	strcpy(filename, LOGDIR);
	strcat(filename, "/");
	strcat(filename, LOGFILE);

	printf("Open log file and append debug info.\n");
	fp = fopen(filename, "a");
	fwrite(info, strlen(info), 1, fp);
	fclose(fp);

	return 0;
}
